import { useState } from 'react'

// useState va a recibir un unico argumento que sera el valor
// que tendra el contador

// reglas de los hooks:
// 1. no se llaman en los loops, siempre se deben llamar en el nivel
// mas alto del componente.
// 2. solo se llama en dos partes:
// Componentes de react
// custom hooks
// cuando creemos un custom hook use"nombre que le quiere dar"

// class App extends Component {
//   state = { contador: 0 }
//   incrementar = () => {
//     this.setState({ contador: this.state.contador + 1})
//   }
//   render() {
//     return (
//       <div>
//         contador: {this.state.contador}
//         <button onClick={this.incrementar}>Incrementar</button>
//       </div>
//     )
//   }
// }

const useContador = (inicial) => {
  const [contador, setContador] = useState(inicial)
  const incrementar = () => {
    setContador(contador + 1)
  }

  return [contador, incrementar]
}

const App = () => {
  const [contador, incrementar] = useContador(0)
  return (
    <div>
      Contador: {contador}
      <button onClick={incrementar}>Incrementar</button>
    </div>
  )
}



export default App


