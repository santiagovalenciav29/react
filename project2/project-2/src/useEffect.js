import { useState, useEffect } from "react";

//useEffect recibe dos argumentos, el primero es una funcion
// con la logica que este va a ejecutar y el siguiente son las dependencias
// que estes necesita para ejecutarse en caso de que solo queramos de cambie una vez le pasamos
//como segundo argumento un array vacio

const useContador = (inicial) => {
  const [contador, setContador] = useState(inicial);
  const incrementar = () => {
    setContador(contador + 1);
  };

  return [contador, incrementar];
};

const Interval = ({ contador }) => {
  useEffect(() => {
    const i = setInterval(() => console.log(contador), 1000);
    return () => clearInterval(i);
  }, [contador]);
  return <p>Intervalo</p>;
};
const App = () => {
  const [contador, incrementar] = useContador(0);
  useEffect(() => {
    document.title = contador;
  }, [contador]);
  return (
    <div>
      Contador: {contador}
      <button onClick={incrementar}>Incrementar</button>
      <Interval contador={contador} />
    </div>
  );
};

export default App;
