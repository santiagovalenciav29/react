const impura = () => new Date().toLocaleDateString();

console.log(impura());

// la diferencia entre una funcion pura a una impura es que
// cuando se le llama a una funcion de impura puede devolver valores
// diferentes.
const MiComponent = ({ miProp }) => {
  return <div>Nombre: {miProp}</div>;
};
const App = () => {
  return <MiComponent miProp={"chanchito Feliz"} />;
};

export default App;

// existen herramientas para agregar estados a los componentes funcionales
// aqui se trabajaran con hooks

// Un hook es una funcion especial que permite "conectarse" a caracteristicas de React. Por
// ejemplo, UseState es un hook que te permite añadir el estado de react a un componente de
// funcion. Mas adelante hablaremos sobre otros hooks.
// si creas un componente de funcion y descubres que necesitas añadirle estado, antes habia que
// crear una clase. Ahora puedes usar un hook dentro de un componente de funcion existente.
// los hooks NO se llaman dentro de ciclos, condicionales o funciones anidadas. Usa siempre
// hooks en el nivel superior de tu funcion en React, antes de cualquier retorno prematuro. asi
// siempre se llamaran en el mismo orden cada vez que un componente se renderiza
//Construir tus propios Hooks te permite extraer la logica del componente en funciones
//reutilizables
