class Monitor {
    static contadorMonitor = 0;
    constructor(marca, tamanio){
        this._MonitorId = ++Monitor.contadorMonitor;
        this._marca = marca;
        this._tamanio = tamanio;
    }

    get monitorId() {
        return this._MonitorId;
    }

    get marca() {
        return this._marca;
    }

    set marca(marca) {
        return this._marca = marca;
    }

    get tamanio() {
        return this._tamanio;
    }

    set tamanio(tamanio) {
        return this._tamanio = tamanio;
    }

    toString(){
        return `
        Monitor: \n
        idMonitor: ${this._MonitorId}
        marca: ${this._marca}
        tamanio: ${this._tamanio}`
    }
}

module.exports = Monitor;