class DispositivoEntrada {
    static cantidadDispositivos = 0;
    constructor(tipoEntrada, marca) {
        this._cantidad = ++DispositivoEntrada.cantidadDispositivos;
        this._tipoEntrada = tipoEntrada;
        this._marca = marca;
    }
    get cantidad() {
        return this._cantidad;
    }

    get tipoEntrada() {
        return this._tipoEntrada;
    }

    set tipoEntrada(tipoEntrada) {
        return this._tipoEntrada = tipoEntrada;
    }

    get marca(){
        return this._marca;
    }

    set marca(marca) {
        return this._marca = marca;
    }

    toString() {
        return `
        cantidad de dispositivos de entrada: ${this._cantidad}
        tipo de entrada : ${this._tipoEntrada}
        marca : ${this._marca}
        `
    }
}

module.exports = DispositivoEntrada;