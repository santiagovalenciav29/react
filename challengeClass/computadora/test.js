const Computadora = require("./computadora")
const Raton = require("./raton.js")
const Teclado = require("./teclado.js")
const dispositivoEntrada = require("./dispositivoEntrada.js")
const Monitor = require("./Monitor.js")
const Orden = require('./Orden.js')



const dispositivoEntrada1 = new dispositivoEntrada('usb','samsung');
// console.log(dispositivoEntrada1)

const Raton1 = new Raton(dispositivoEntrada1.tipoEntrada,dispositivoEntrada1.marca)
// console.log(Raton1)

const Teclado1 = new Teclado(dispositivoEntrada1._tipoEntrada,dispositivoEntrada1.marca)

const Monitor1 = new Monitor('Apple','22"')

const Computadora2 = new Computadora('ensamblada',Monitor1,Raton1,Teclado1)
// console.log(Computadora2)
const Orden1 = new Orden();

Orden1.addComputer(Computadora2);
console.log(Orden1)