class Orden {
    static contadorOrdenes = 0;
    constructor(){
        this._idOrden = ++Orden.contadorOrdenes;
        this._computadoras = [];
    }

    get idOrden() {
        return this._idOrden;
    }

    addComputer(computadora) {
        this._computadoras.push(computadora);
    }

    showOrder() {
        let computadorasOrden = '';

        for (let computadora of this._computadoras){
            compurasOrden += `\n ${computadora}`
        }

        console.log(`
        Orden: ${this._idOrden}
        Computadoras: ${computadorasOrden}`);
    }
}

module.exports = Orden;