const DispositivoEntrada = require('./dispositivoEntrada.js')

class Teclado extends DispositivoEntrada {
    static contadorTeclados = 0
    constructor(marca, tipoEntrada){
        super(tipoEntrada, marca)
        this._idTeclado = ++Teclado.contadorTeclados;
    }
    get idTeclado() {
        return this._idTeclado;
    }

    toString(){
        return `
        id_teclado: ${this._idTeclado}
        marca: ${this._marca}
        tipoEntrada: ${this._tipoEntrada}`
    }
}

module.exports = Teclado;