class Computadora {
    static contadorComputadoras = 0;
    constructor(nombre,monitor, teclado, raton){
        this._cantidadComputadoras = ++Computadora.contadorComputadoras;
        this._nombre = nombre;
        this._monitor = monitor;
        this._teclado = teclado;
        this._raton = raton;
    }

    get cantidadComputadoras() {
        return this._cantidadComputadoras;
    }


    toString(){
        return `
        cantidadComputadoras: ${this._cantidadComputadoras}
        nombre: ${this._nombre}
        monitor: ${this._monitor}
        raton: ${this._raton}
        teclado: ${this._teclado}`
    }
}

module.exports = Computadora;