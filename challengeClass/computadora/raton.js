const DispositivoEntrada = require('./dispositivoEntrada.js');

class Raton extends DispositivoEntrada {

    static contadorRatones = 0;

    constructor(tipoEntrada, marca){
        super(tipoEntrada, marca)
        this._idRaton = ++Raton.contadorRatones;
    }
    get idRaton() {
        return this._idRaton;
    }

    toString(){
        return`
        id_Raton: ${this._idRaton}
        tipo Entrada: ${this._tipoEntrada}
        Marca: ${this._marca}`
    }
}

module.exports = Raton;