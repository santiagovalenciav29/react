const Persona = require('./persona');

// para crear una clase hija se pone el nombre de la clase hija extends "nombre de la clase Padre"

class Empleado extends Persona {

    // este metodo estatico funcionara como contador para el id de cada empleado
    static contadorEmpleados = 0;

    constructor (nombre, apellido, edad, sueldo){
        // con el metodo super nos traemos los atributos de la clase padre
        super(nombre,apellido,edad)
        this._idEmpleado = ++Empleado.contadorEmpleados;
        this._sueldo = sueldo
    }

    // para el id solo get ya que solo lo veremos

    get idEmpleado() {
        return this._idEmpleado;
    }

    // para el sueldo si necesitamos el set para ingresarle datos y el get para ver
    get sueldo() {
        return this._sueldo;
    }

    set sueldo(sueldo) {
        return this._sueldo = sueldo;
    }
// en toString ponemos el metodo superpara heredar de la clase padre
    toString() {
        return `
        ${super.toString()}
        id tipo empleado: ${this._idEmpleado}
        sueldo: ${this._sueldo}`;
    }
}

module.exports = Empleado;