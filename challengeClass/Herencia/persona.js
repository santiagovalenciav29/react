class Persona {
    static contadorPersonas = 0;

    constructor(nombre, apellido, edad) {

        this._idPersona = ++Persona.contadorPersonas;
        this._nombre = nombre;
        this._apellido = apellido;
        this._edad = edad;
    }

    get idPersona() {
        return this._idPersona;
    }

    get nombre() {
        return this._nombre;
    }

    set nombre(nombre) {
        return this._nombre = nombre;
    }

    get apellido() {
        return this._apellido;
    }

    set apellido(apellido) {
        return this._apellido = apellido;
    }

    get edad() {
        return this._edad;
    }

    set edad(edad) {
        return this._edad = edad;
    }



    toString() {
        return `
        id: ${this._idPersona}
        nombre: ${this.nombre}
        apellido: ${this.apellido}
        edad: ${this._edad}`
    }
}

// set es para modificar los elementos de la clase y el get es solo para leer

// iniciar el metodo string


module.exports = Persona;