const Persona = require('./persona.js')

class cliente extends Persona {

    static contadorClientes = 0;
    constructor(nombre, apellido, edad, fechaRegistro) {
        super(nombre,apellido,edad)
        this._idCliente = ++cliente.contadorClientes;
        this._fechaRegisto = fechaRegistro;
    }

    get idCliente() {
        return this._idCliente;
    }

    get fechaRegistro() {
        return this._fechaRegisto;
    }

    set fechaRegistro(fechaRegistro){
        return this._fechaRegisto = fechaRegistro;
    }

    toString(){
        return `
        ${super.toString()}
        id Cliente: ${this._idCliente}
        fecha de Registro: ${this._fechaRegisto}`;
    }
}

module.exports = cliente;