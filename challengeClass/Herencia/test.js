const Persona = require('./persona.js');

const Empleado = require('./empleado');

const Cliente = require('./cliente.js');

let Persona1 = new Persona('Evangelino', 'Perez', 34)
// console.log(Persona1.toString());


let Empleado1 = new Empleado(Persona1.nombre, Persona1.apellido, Persona1.edad, 2500000)

// console.log(Empleado1.toString());

let cliente1 = new Cliente(Persona1.nombre, Persona1.apellido, Persona1._edad, new Date(2021,07,31))

console.log(cliente1)
