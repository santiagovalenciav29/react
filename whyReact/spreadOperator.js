const fn = (a,b,c) => console.log(a + b + c)

const arr = [1, 2, 3]

fn(...arr)

//argumento es el valor que contiene el parametro, para usar spread debemos estar seguro de que almenos
//se estan pasando todos los parametros necesarios

const obj1 = { a:1 , b:2 }

const obj2 = { b: 5, c: 'chanchito Feliz' }

const obj3 = {...obj1 }

console.log(obj3)
obj1.a = 10
console.log(obj3,obj1)

// cuando spreadOperator copia, copa de izquierda a derecha

const rest = (a, ...argumentos) =>{
    console.log(a)
    console.log(argumentos)
}

rest(4,1,2,3)

// en el caso de los spreadOperators en funciones, se deben poner al final

