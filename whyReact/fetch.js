const url = 'http://jsonplaceholder.typicode.com/users'


// el body lo usaremos para enviar datos al servidor

// para indentificacion y tipo de contenido a enviar usaremos las cabeceras

const fn = async () =>{

    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer acadebeireltoquendeautorizacion'
        },
        body: JSON.stringify({
            name: 'chanchito feliz',
            website: 'google.com'
        })
    })
    const data = await response.json();
    console.log(data)
}

fn()

// no se debe usar async/ await en programacion funcional ya que async/awwait seria codigo estrictamente imperativo