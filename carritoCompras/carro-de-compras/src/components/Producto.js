import { Component } from 'react'
import Button from './Button'

const styles = {
    producto: {
        border: 'solid 1px #eee',
        //borde gris
        boxShadow: '0 5px 5px rgb(0, 0, 0, 0.1)',
        //para añadir sombreado
        width: '30%',
        //ancho de las tarjetas
        padding: '10px 15px',
        //para que el contenido se aleje de los bordes
        borderRadius: '5px',
        //para redondear los bordes
    },
    img: {
        width: '100%',
    }
}

class Producto extends Component {
    render() {
        const { producto , agregarAlCarro } = this.props
        return (
            <div style={styles.producto}>
                <img style={styles.img} alt={producto.name} src={producto.img}/>
                <h3>{producto.name}</h3>
                <p>{producto.price}</p>
                <Button onClick={() => agregarAlCarro(producto)}>
                    Agregar al carro
                </Button>
            </div>
        )
    }
}

export default Producto