import { Component } from "react";

const styles = {
    layout: {
        //para forzar el color de fondo en este caso blanco
        backgroundColor: '#fff',
        // Con este le damos un color a la fuente
        color: '#0A283E',
        alignItems: 'center',
        // el flex se le pone para que este layout pueda ser aplicado
        display: 'flex',
        // ahora para posicionar las imagenes
        flexDirection: 'column',
    },
    container:{
        width:'1200px',
    }
}
class layout extends Component {
    render() {
        return (
            // con el primer el div es para centrar todo el contenido
            //con el segundo le daremos un ancho en cuanto a los pixeles
            <div style={styles.layout}>
                <div style={styles.container}>
                    {this.props.children}
                lala
                </div>
            </div>
        )
    }
}

export default layout